package imported;

public class Pattern55 {
    public static void main(String[] args) {
        int line=9;
        int no=1;
        for(int i=0;i<line;i++)
        {
            int start=1;
            for(int j=0;j<no;j++)
            {
                System.out.print(start++);
            }
            System.out.println();
            if(i<=3)
            {
                no++;
            }
            else {
                no--;
            }
        }
    }
}
