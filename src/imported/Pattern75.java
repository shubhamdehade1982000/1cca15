package imported;

public class Pattern75 {
    public static void main(String[] args) {
        int line=5;
        int no=1;
        int spaces=2;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<no;j++)
            {
                System.out.print("*");
            }
            for(int k=0;k<spaces;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<no;j++)
            {
                System.out.print("*");
            }
            System.out.println();
            no++;
        }
    }
}
