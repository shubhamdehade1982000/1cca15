package imported;

public class Pattern71 {
    public static void main(String[] args) {
        int line=5;
        int no=1;
        for(int i=0;i<line;i++)
        {
            int start=1;
            for(int j=0;j<no;j++)
            {
                if(j<i)
                {
                    System.out.print(start++);
                }
                else {
                    System.out.print(start--);
                }
            }
            System.out.println();
            no+=2;
        }
    }
}
