package programming;

public class Pattern18 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int num=1;
        for(int j=0;j<line;j++){
            for(int i=0;i<star;i++){
                if(j%2==0)
                System.out.print(num);
                else
                    System.out.print("*");
            }
            System.out.println();
            num++;
        }
    }
}
