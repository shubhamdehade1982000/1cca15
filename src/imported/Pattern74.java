package imported;

public class Pattern74 {
    public static void main(String[] args) {
        int line=5;
        int no=1;
        int spaces=4;
        for(int i=0;i<line;i++)
        {
            char start='A';
            for(int k=0;k<spaces;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<no;j++)
            {
                if(j<i)
                {
                    System.out.print(start++);
                }
                else {
                    System.out.print(start--);
                }
            }
            System.out.println();
            no+=2;
            spaces--;
        }
    }
}
