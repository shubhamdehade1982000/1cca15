package imported;

public class Pattern58 {
    public static void main(String[] args) {
        int line=9;
        int no=5;
        for(int i=0;i<line;i++)
        {
            int start=1;
            for(int j=0;j<no;j++)
            {
                System.out.print(start++);
            }
            if(i<=3)
            {
                no--;
            }
            else {
                no++;
            }
            System.out.println();
        }
    }
}
