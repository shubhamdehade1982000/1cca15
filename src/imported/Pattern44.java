package imported;

public class Pattern44 {
    public static void main(String[] args) {
        int line=5;
        int spaces=4;
        int start=1;
        int no=1;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<no;k++)
            {
                System.out.print(start +" ");
            }
            System.out.println();
            start++;
            spaces--;
            no++;
        }
    }
}
