package programming;

public class Pattern11 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        char ch='A';
        for(int j=0;j<line;j++){
            for(int i=0;i<star;i++){
                System.out.print(ch++);
            }
            System.out.println();
        }

    }
}
