package imported;

public class Pattern38 {
    public static void main(String[] args) {


        int line = 5;
        int start = 1;
        for (int i = 0; i < line; i++) {
            int no=1;
            for (int j = 0; j < start; j++) {
                System.out.print(no++);
            }
            System.out.println();
            start++;
        }
    }

}
