package imported;

public class Pattern93 {
    public static void main(String[] args) {
        int line=5;
        int star=9;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                if(i==j || i+j==8)
                {
                    System.out.print('N');
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
