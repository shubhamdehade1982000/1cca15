package imported;

public class Pattern51 {
    public static void main(String[] args) {
        int line=9;
        int start=1;

        int spaces=4;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<start;k++)
            {
                System.out.print(start +" ");
            }
            System.out.println();
            if(i<=3)
            {
                spaces--;
                start++;
            }
            else {
                spaces++;
                start--;
            }
        }
    }
}
