package programming;

public class Pattern13 {
    public static void main(String[] args) {
        int line = 4;
        int star = 4;
        char ch = 'A';

        for (int j = 0; j < line; j++) {
            for(int i=0;i<star;i++){
                System.out.print(ch++);
                if(ch>'E')
                    ch='A';
            } System.out.println();

        }
    }
}
