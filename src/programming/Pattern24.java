package programming;

public class Pattern24 {
    public static void main(String[] args) {
        int star=1;
        int space=2;
        int line=5;

        for(int i=0;i<line;i++){
            for(int j=0;j<space;j++){

                System.out.print(" ");
            }
            for(int k=0;k<star;k++){
                if(k==0||k==star-1){
                System.out.print("*");
            }
            else {
                    System.out.print(" ");}
            }
            System.out.println();
            if(i<=1){
                space--;
                star+=2;
            }else {
                space++;
                star-=2;

            }

        }

    }
}
