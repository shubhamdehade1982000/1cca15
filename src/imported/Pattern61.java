package imported;

public class Pattern61 {
    public static void main(String[] args) {
        int line = 5;

        int no = 1;
        for (int i = 0; i < line; i++) {
            for (int j = 0; j < no; j++) {
                System.out.print("*");
            }
            System.out.println();
            no+=2;
        }
    }
}
