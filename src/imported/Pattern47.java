package imported;

public class Pattern47 {
    public static void main(String[] args) {


        int line = 9;
        int no = 1;
        for (int i = 0; i < line; i++) {
            int start = 9;
            for (int j = 0; j < no; j++) {
                System.out.print(start--);
            }
            System.out.println();
            no++;
        }
    }
}