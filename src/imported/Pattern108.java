package imported;

public class Pattern108 {
    public static void main(String[] args) {
        int line=5;
        int star=1;
        int no=5;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<no;j++)
            {
                if(i==j)
                {
                    System.out.print(star++);
                }
                else
                {
                    System.out.print(0);
                }
            }
            System.out.println();
        }
    }
}
