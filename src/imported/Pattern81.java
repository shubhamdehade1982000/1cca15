package imported;

public class Pattern81 {
    public static void main(String[] args) {
        int line=5;

        int no=1;
        int start1=1;
        int spaces=8;
        for(int i=0;i<line;i++)
        {
            int start=1;
            int start2=start1;
            for(int j=0;j<no;j++)
            {
                System.out.print(start++);
            }
            for(int k=0;k<spaces;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<no;j++)
            {
                System.out.print(start2--);
            }

            start1++;
            spaces-=2;
            no++;

            System.out.println();
        }
    }
}
