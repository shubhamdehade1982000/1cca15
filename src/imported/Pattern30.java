package imported;

public class Pattern30 {
    public static void main(String[] args) {


        int line = 12;
        int start = 6;
        char ch = 'F';
        for (int i = 0; i < line; i++) {
            char ch1 = ch;
            for (int j = 0; j < start; j++) {
                System.out.print(ch1--);
            }
            System.out.println();
            if (i < 5) {
                start--;
                ch--;
            } else if (i > 5) {

                start++;
                ch++;
            }
        }

    }

}
