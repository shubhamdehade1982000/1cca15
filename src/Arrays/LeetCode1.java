package Arrays;

import java.util.Arrays;

public class LeetCode1 {
     public static void additionArray() {
         int[] num = {1, 2, 3, 4, 5};

         for (int i = 1; i < num.length; i++)
             num[i] += num[i - 1];

         System.out.print(Arrays.toString(num));
     }
    public static void main(String[] args) {
        additionArray();

    }
}