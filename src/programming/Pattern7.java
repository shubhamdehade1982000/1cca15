package programming;

public class Pattern7 {
    public static void main(String[] args) {
        int line=4;
        int star=4;
        for(int j=0;j<line;j++){
            for(int i=0;i<star;i++){
                if(i==j||i+j==3)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
    }
}
