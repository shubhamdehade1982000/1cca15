package imported;

public class Pattern78 {
    public static void main(String[] args) {
     int line=5;
     int star1=1;
     int star2=5;
     int spaces1=4;
     int spaces2=0;
     for(int i=0;i<line;i++)
     {
         for(int j=0;j<star1;j++)
         {
             System.out.print("*");
         }
         for(int k=0;k<spaces1;k++)
         {
             System.out.print(" ");
         }
         for(int j=0;j<star2;j++)
         {
             System.out.print("*");
         }
         for(int k=0;k<spaces2;k++)
         {
             System.out.print(" ");
         }
         for(int j=0;j<star2;j++)
         {
             System.out.print("*");
         }
         for(int k=0;k<spaces1;k++)
         {
             System.out.print(" ");
         }
         for(int j=0;j<star1;j++)
         {
             System.out.print("*");
         }
         System.out.println();
         star1++;
         star2--;
         spaces1--;
         spaces2+=2;
     }
    }
}
