package imported;

public class Pattern13 {
    public static void main(String[] args) {
        int line=5;
        int start=1;
        int no=2;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<no;j++)
            {
                if(i%2==0)
                {
                    System.out.print(start);
                }
                else {
                    System.out.print("*");
                }
            }
            System.out.println();
            start++;
            no++;
        }
    }
}
