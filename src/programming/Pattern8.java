package programming;

public class Pattern8 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int num=1;
        for(int j=0;j<line;j++){
            for(int i=0;i<star;i++){
                System.out.print(num);
            }
            System.out.println();
            num++;
        }
    }
}
