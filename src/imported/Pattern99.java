package imported;

public class Pattern99 {
    public static void main(String[] args) {
        int line=9;
        int star=5;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                if(j==0 || i==j || i+j==8)
                {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
