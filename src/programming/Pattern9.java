package programming;

public class Pattern9 {
    public static void main(String[] args) {

        int line=3;
        int star=5;
        for(int j=0;j<line;j++){
            int num=1;
            for(int i=0;i<star;i++){
                System.out.print(num);
                num++;
            }
            System.out.println();
        }
    }
}
