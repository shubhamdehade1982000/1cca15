package imported;

public class Pattern56 {
    public static void main(String[] args) {
        int line=5;
        int no=1;
        int spaces=4;
        for(int i=0;i<line;i++)
        {
            int start=1;
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<no;k++)
            {
                System.out.print(start++ +" ");
            }
            System.out.println();
            spaces--;
            no++;
        }
    }
}
