package imported;

public class Pattern5 {
    public static void main(String[] args) {
        int line=9;
        int start=1;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<start;j++)
            {
                System.out.print("*");
            }
            System.out.println();
            if(i<=3)
            {
                start++;
            }
            else {
                start--;
            }
        }
    }
}
