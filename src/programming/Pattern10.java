package programming;

public class Pattern10 {
    public static void main(String[] args) {
        int line=4;
        int star=5;
        for(int j=0;j<line;j++){
            int num=5;
            for(int i=0;i<star;i++){
                System.out.print(num--);

            }
            System.out.println();
        }
    }
}
