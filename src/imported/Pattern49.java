package imported;

public class Pattern49 {
    public static void main(String[] args) {
        int line=9;
        int spaces=8;
        int no=1;
        for(int i=0;i<line;i++)
        {
            int start=1;
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<no;k++)
            {
                System.out.print(start++);
            }
            System.out.println();
            spaces--;
            no++;
        }
    }
}
