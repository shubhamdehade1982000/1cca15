package programming;

public class Pattern5 {
    public static void main(String[] args) {
        int line=5;
        int star=5;

        for(int j=0;j<line;j++){
            for(int i=0;i<star;i++){
                if(j==0||i==4||i==j)
                System.out.print(" * ");
                else
                    System.out.print("   ");

            }
            System.out.println();
        }
    }
}
