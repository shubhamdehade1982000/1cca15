package imported;

public class Pattern43 {
    public static void main(String[] args) {
        int line=5;
        int start=5;
        int spaces=4;
        int no=1;
        for(int i=0;i<line;i++)
        {
            int start1=start;
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<no;k++)
            {
                System.out.print(start1++ +" ");
            }
            System.out.println();
            start--;
            spaces--;
            no++;
        }
    }
}
