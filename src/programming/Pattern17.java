package programming;

public class Pattern17 {
    public static void main(String[] args) {
        int line=4;
        int star=4;
        int ch1=1;
        for(int j=0;j<line;j++){
            int ch2=1;
            for(int i=0;i<star;i++){
                System.out.print(" "+ch2++ *ch1+"   ");

            }
            System.out.println();
           ch1++;
        }
    }
}
