package imported;

public class Pattern101 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int spaces=4;
        for(int i=0;i<line;i++)
        {
            for(int k=0;k<spaces;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<star;j++)
            {
                if(i==0 || i+j==4 || i==4  )
                System.out.print("*");


            }
            System.out.println();
            spaces--;
        }
    }
}
