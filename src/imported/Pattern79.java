package imported;

public class Pattern79 {
    public static void main(String[] args) {
        int line=9;
        int start=1;
        int spaces=8;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<start;j++)
            {
                System.out.print("*");
            }
            for(int k=0;k<spaces;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<start;j++)
            {
                System.out.print("*");
            }
            if(i<=3)
            {
                start++;
                spaces-=2;
            }
            else {
                start--;
                spaces+=2;
            }
            System.out.println();
        }
    }
}
