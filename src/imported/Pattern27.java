package imported;

public class Pattern27 {
    public static void main(String[] args) {


        int line = 9;
        int start = 1;
        char ch='A';
        for (int i = 0; i < line; i++) {

            for (int j = 0; j < start; j++) {
                System.out.print(ch);
            }
            System.out.println();
            if(i<=3)
            {
                ch++;
                start++;
            }
            else {
                ch--;
                start--;
            }
        }
    }


}
