package Arrays;
import java.util.Scanner;

public class ArrayDemo1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("ENTER TOTAL NO OF SUBJECTS");
        int size = sc1.nextInt();

        double sum = 0.0;
        double[] marks = new double[size];
        for (int a = 0; a < size; a++) {
            System.out.println("ENTER MARKS");
            marks[a] = sc1.nextDouble();
            sum += marks[a];

        }
        System.out.println("TOTAL MARKS :" + sum);
        System.out.println("PERCENTAGE :" + sum / size);
    }
}
