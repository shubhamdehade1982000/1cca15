package imported;

public class Pattern73 {
    public static void main(String[] args) {
        int line=5;
        int no=1;
        for(int i=0;i<line;i++)
        {
            char ch='A';
            for(int j=0;j<no;j++)
            {
                if(j<i)
                {
                    System.out.print(ch++);
                }
                else {
                    System.out.print(ch--);
                }
            }
            System.out.println();
            no+=2;
        }
    }
}
