package imported;

public class Pattern98 {
    public static void main(String[] args) {
        int line=9;
        int star=9;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<star;j++)
            {
                if(i+j==4 || j-i==4 || i-j==4 || i+j==12)
                {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
