package imported;

public class Pattern59 {
    public static void main(String[] args) {
        int line=9;
        int no=5;
        int spaces=0;
        for(int i=0;i<line;i++)
        {
            int start=1;
            for(int k=0;k<spaces;k++)
            {
                System.out.print(" ");
            }
            for(int j=0;j<no;j++)
            {
                System.out.print(start++);
            }
            if(i<=3)
            {
                no--;
                spaces++;
            }
            else {
                no++;
                spaces--;
            }
            System.out.println();
        }
    }
}
