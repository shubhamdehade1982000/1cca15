package imported;

public class Pattern4 {
    public static void main(String[] args) {
        int line=9;
        int start=5;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<start;j++)
            {
                System.out.print("*");
            }
            System.out.println();
            if(i<=3)
            {
                start--;
            }
            else {
                start++;
            }
        }
    }
}
