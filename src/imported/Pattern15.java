package imported;

public class Pattern15 {
    public static void main(String[] args) {
        int line=9;
        int start=5;
        int spaces=0;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<start;k++)
            {
                System.out.print("*");
            }
            System.out.println();
            if(i<=3)
            {
                start--;
                spaces++;
            }
            else if(i>3)
            {
                spaces=4;
                start++;
            }
        }
    }
}
