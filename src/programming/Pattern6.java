package programming;

public class Pattern6 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        for(int j=0;j<line;j++){
            for(int i=0;i<star;i++){
                if(j==2||i==2)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
    }
}
