package programming;

public class Pattern21 {
    public static void main(String[] args) {
        int line=5;
        int space=line-1;
        int star=1;

        for(int j=0;j<line;j++){
            int number=1;
            for(int k=0;k<space;k++){
                System.out.print(" ");
            }
            for(int l=number;l<=star;l++){
                System.out.print(number++);
            }
            System.out.println();
            space--;
            star++;

        }
    }
}
