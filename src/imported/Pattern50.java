package imported;

public class Pattern50 {
    public static void main(String[] args) {
        int line=7;
        int no=30;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<no;j++)
            {
                if(j<=2)
                {
                    System.out.print(0);
                }
                else if(j>=3 && j<=5)
                {
                    System.out.print(1);
                }
                else if(j>=6 && j<=8)
                {
                    System.out.print(2);
                }
                else if(j>=9 && j<=11)
                {
                    System.out.print(3);
                }
                else if(j>=12 && j<=14)
                {
                    System.out.print(4);
                }
                else if(j>=15 && j<=17)
                {
                    System.out.print(5);
                }
                else if(j>=18 && j<=20)
                {
                    System.out.print(6);
                }
                else if(j>=21 && j<=23)
                {
                    System.out.print(7);
                }
                else if(j>=24 && j<=26)
                {
                    System.out.print(8);
                }
                else if(j>=27 && j<=29)
                {
                    System.out.print(9);
                }
            }
            System.out.println();
        }
    }
}
