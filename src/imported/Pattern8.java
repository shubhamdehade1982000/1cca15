package imported;

public class Pattern8 {
    public static void main(String[] args) {
        int line=5;
        int start=5;
        int spaces=1;
        for(int i=0;i<line;i++)
        {
            for(int j=0;j<spaces;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<start;k++)
            {
                System.out.print("*");
            }
            System.out.println();
            spaces++;
            start--;
        }
    }
}
