package imported;

public class Pattern60 {
    public static void main(String[] args) {
        int line=9;
        int start=1;
        int space=0;
        int no=5;
        for(int i=0;i<line;i++)
        {
            int start1=start;
            for(int j=0;j<space;j++)
            {
                System.out.print(" ");
            }
            for(int k=0;k<no;k++)
            {
                System.out.print(start1++ +" ");
            }
            if(i<=3)
            {
                space++;
                start++;
                no--;
            }
            else {
                space--;
                start--;
                no++;
            }
            System.out.println();
        }
    }
}
