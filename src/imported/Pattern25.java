package imported;

public class Pattern25 {
    public static void main(String[] args) {


        int line = 5;
        int start = 1;
        for (int i = 0; i < line; i++) {
            char ch = 'A';
            for (int j = 0; j < start; j++) {
                System.out.print(ch++);
            }
            System.out.println();
            start++;
        }
    }
}
